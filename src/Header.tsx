import React from "react";
import { Link, withRouter } from "react-router-dom";
// import Main from "./pages/Main";
import ReleaseEdit from "./pages/ReleaseEdit";


import { Consumer, ContextInterface as ctx } from "./Store";

interface IHeader {
    search: string;
}

class Header extends React.Component<any, IHeader> {
    constructor(props: any) {
        super(props);

        this.state = {
            search: ""
        };
    }

    private handleChange = (e: any): void => {
        this.setState({[e.target.name]: e.target.value} as any);
    };

    private viewSearch(): boolean {
        if (
            this.props.match.params.id === "releaseedit" ||
            this.props.match.params.id === "sneakersedit" ||
            this.props.match.params.id === "sneakers"
        ) {
            return true;
        } else {
            return false;
        }
    }

    private handleSubmit = (e: any): void => {
        e.preventDefault();

        if (this.state.search === "") {
            this.props.history.push(`/sneakersedit`);
        } else {
            this.props.history.push(`/searchredirect/${this.state.search}`);
        }

        this.setState({search: ""});
    };

    public render() {
        return (
            <div>
                <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                    <Link
                        to="/"
                        className="navbar-brand col-sm-3 col-md-2 mr-0 bold"
                        href="#"
                    >
                        PAIR2 ADMIN
                    </Link>

                    {this.viewSearch() && (
                        <form className="flex-md-nowrap w-100">
                            <input
                                onChange={this.handleChange}
                                name="search"
                                className="form-control form-control-dark w-search inline-block"
                                type="text"
                                placeholder="Search"
                                aria-label="Search"
                            />
                            <button
                                onClick={this.handleSubmit}
                                className="btn btn-sm btn-dark btn-search w-200px ml-1 inline-block"
                            >
                                검색
                            </button>
                        </form>
                    )}

                    <ul className="navbar-nav px-3">
                        <li className="nav-item text-nowrap">
                            {/*<Link to={"/"} className="nav-link white">SignOut</Link>*/}
                            {/*<Link to={"/signin"} className="nav-link white">SignIn</Link> */}
                        </li>
                    </ul>
                </nav>

                <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                    <div className="sidebar-sticky">
                        <ul className="nav flex-column">
                            <li className="nav-item">
                                <Link
                                    to="/releaseedit"
                                    className={`nav-link ${
                                        this.props.match.params.id === "releaseedit" && "active" ||
                                        this.props.match.params.id === undefined && "active"
                                    }`}
                                >
                                    런칭캘린더
                                </Link>
                            </li>
                        </ul>


                        {/*<ul className="nav flex-column">
                            <li className="nav-item">
                                <Link
                                    to="/"
                                    className={`nav-link ${
                                        this.props.match.params.id === undefined && "active"
                                    }`}
                                >
                                    Main Dashboard
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link
                                    to="/sneakers"
                                    className={`nav-link ${
                                        this.props.match.params.id === "sneakers" && "active"
                                    }`}
                                >
                                    스니커
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/sneakersedit"
                                      className={`nav-link ${this.props.match.params.id === "sneakersedit" && "active"}`}>
                                    Sneaker Edit
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/release"
                                      className={`nav-link ${this.props.match.params.id === "release" && "active"}`}>
                                    런칭캘린더
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link
                                    to="/releaseedit"
                                    className={`nav-link ${
                                        this.props.match.params.id === "releaseedit" && "active"
                                    }`}
                                >
                                    런칭캘린더
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/releaseedit/"
                                      className={`nav-link ${this.props.match.params.id === "releaseedit" && "active"}`}>
                                    Release Calendar Edit
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link
                                    to="/trade"
                                    className={`nav-link ${
                                        this.props.match.params.id === "trade" && "active"
                                    }`}
                                >
                                    거래내역관리
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link
                                    to="/adjustment"
                                    className={`nav-link ${
                                        this.props.match.params.id === "adjustment" && "active"
                                    }`}
                                >
                                    정산
                                </Link>
                            </li>

                            <li className="nav-item">
                                <Link to="/users"
                                      className={`nav-link ${this.props.match.params.id === "users" && "active"}`}>
                                    User List
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/qna"
                                      className={`nav-link ${this.props.match.params.id === "qna" && "active"}`}>
                                    Q/A (질문 모니터링)
                                </Link>
                            </li>
                        </ul>*/}

                        {/*<h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>etc config</span>
                            <a
                                className="d-flex align-items-center text-muted"
                                href="#"
                                aria-label="Add a new report"
                            >
                                <span data-feather="plus-circle"></span>
                            </a>
                        </h6>

                        <ul className="nav flex-column">
                            <li className="nav-item">
                                <Link
                                    to="/faq"
                                    className={`nav-link ${
                                        this.props.match.params.id === "faq" && "active"
                                    }`}
                                >
                                    FAQ
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link
                                    to="/filters"
                                    className={`nav-link ${
                                        this.props.match.params.id === "filters" && "active"
                                    }`}
                                >
                                    브랜드 필터 추가
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link
                                    to="/commision"
                                    className={`nav-link ${
                                        this.props.match.params.id === "commision" && "active"
                                    }`}
                                >
                                    수수료설정
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link
                                    to="/coupon"
                                    className={`nav-link ${
                                        this.props.match.params.id === "coupon" && "active"
                                    }`}
                                >
                                    쿠폰
                                </Link>
                            </li>
                        </ul>*/}

                        <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>회원관리</span>
                            <a
                                className="d-flex align-items-center text-muted"
                                href="#"
                                aria-label="Add a new report"
                            >
                                <span data-feather="plus-circle"></span>
                            </a>
                        </h6>
                        <ul className="nav flex-column">
                            <li className="nav-item">
                                <Link
                                    to="/userList"
                                    className={`nav-link ${
                                        this.props.match.params.id === "userList" && "active"
                                    }`}
                                >
                                    회원 리스트
                                </Link>
                            </li>
                        </ul>

                    </div>
                </nav>
                {this.props.match.params.id === undefined && <ReleaseEdit/>}
            </div>
        );
    }
}

export default withRouter(Header);
