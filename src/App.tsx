import React from "react";
import Navigation from "./Navigation";
import { Provider, ContextInterface } from "./Store";

class App extends React.Component<any, ContextInterface> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  public componentDidMount() {}

  public render() {
    return (
      <Provider>
        <Navigation />
      </Provider>
    );
  }
}

export default App;
