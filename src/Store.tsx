import * as React from "react";

export interface ContextInterface {
}

export const ctxt: any = React.createContext<ContextInterface | null>(null);
export const Provider = ctxt.Provider;
export const Consumer = ctxt.Consumer;