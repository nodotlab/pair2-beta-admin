import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./Header";
import Signin from "./pages/Signin";
import SneakersAdd from "./pages/SneakersAdd";
import SneakersEdit from "./pages/SneakersEdit";
import Trade from "./pages/Trade";
import ReleaseAdd from "./pages/ReleaseAdd";
import ReleaseEdit from "./pages/ReleaseEdit";
import Game from "./pages/Game";
import GameEdit from "./pages/GameEdit";
import Users from "./pages/Users";
import Faq from "./pages/Faq";
import Commision from "./pages/Commision";
import Filters from "./pages/Filters";
import Qna from "./pages/Qna";
import Coupon from "./pages/Coupon";
import Adjustment from "./pages/Adjustment";
import SearchRedirect from "./components/SearchRedirect";
import userList from "./pages/UserList";

class Navigation extends React.Component<any> {
    public render() {
        return (
            <Router>
                <Route path="/releaseedit/" exact component={ReleaseEdit}/>
                <Route path="/:id" render={({match}) => <Header match={match}/>}/>
                <Route path="/" exact component={Header}/>
                <Route path="/signin" exact component={Signin}/>
                <Route path="/searchredirect/:search" exact component={SearchRedirect} />
                {/* <Route path="/sneakers" exact component={SneakersAdd} /> */}
                <Route path="/sneakers" exact component={SneakersEdit}/>
                <Route path="/sneakersedit/" exact component={SneakersEdit}/>
                <Route path="/sneakersedit/:search" exact component={SneakersEdit}/>
                <Route path="/release" exact component={ReleaseAdd}/>
                <Route path="/releaseedit/:id" exact component={ReleaseEdit}/>
                <Route path="/adjustment" exact component={Adjustment}/>
                <Route path="/game" exact component={Game}/>
                <Route path="/gameedit" exact component={GameEdit}/>
                <Route path="/users" exact component={Users}/>
                <Route path="/faq" exact component={Faq}/>
                <Route path="/commision" exact component={Commision}/>
                <Route path="/filters" exact component={Filters}/>
                <Route path="/qna" exact component={Qna}/>
                <Route path="/coupon" exact component={Coupon}/>
                <Route path="/trade" exact component={Trade}/>
                <Route path="/userList" exact component={userList}/>
            </Router>
        );
    }
}

export default Navigation;
