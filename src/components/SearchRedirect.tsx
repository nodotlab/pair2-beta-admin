import React from "react";
import { Redirect } from "react-router-dom";


class SearchRedirect extends React.Component<any> {
  public render() {
    return (
      <Redirect to={`/sneakersedit/${this.props.match.params.search}`} />
    );
  }
}


export default SearchRedirect;