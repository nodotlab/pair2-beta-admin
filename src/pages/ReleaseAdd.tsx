import React from "react";
import { timeFormat2 } from "../Method";
import { addRelease, uploadImage } from "../database/Actions";

interface IReleaseAdd {
  file: any;
  fileUrl: string;
  selectIndex: number;
  modal: boolean;
  inputindex: number;
  inputname: string;
  inputbrand: string;
  inputreleaseDate: number;
  inputyear: string;
  inputmonth: string;
  inputdate: string;
  inputUrl: string;
}

class ReleaseAdd extends React.Component<any, IReleaseAdd> {
  constructor(props: any) {
    super(props);
    this.state = {
      file: undefined,
      fileUrl: "",
      selectIndex: -1,
      modal: false,
      inputindex: 0,
      inputname: "",
      inputbrand: "",
      inputreleaseDate: 0,
      inputyear: "",
      inputmonth: "",
      inputdate: "",
      inputUrl: ""
    };
  }

  private handleChange = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private fileRef: any = React.createRef();
  private handleFileRef = (): void => this.fileRef.current.click();

  private handleFile = (e: any): void => {
    this.setState({
      file: e.target.files[0],
      fileUrl: URL.createObjectURL(e.target.files[0])
    });
  };

  private async imageUpload(file: any, size: string) {
    const formData = new FormData();
    formData.append("photos", file);

    const response = await uploadImage(formData, size);
    return response.result.Location;
  }

  private handleSubmit = async () => {
    if (this.state.fileUrl !== "") {
      this.setState({
        inputUrl: await this.imageUpload(this.state.file, "260")
      });
    }

    const data = {
      brand: this.state.inputbrand,
      name: this.state.inputname,
      releaseDate: new Date(
        parseInt(this.state.inputyear),
        parseInt(this.state.inputmonth) - 1,
        parseInt(this.state.inputdate)
      ).getTime(),
      imageUrl: this.state.inputUrl ? this.state.inputUrl : false
    };

    await addRelease(data);
    alert("업로드완료");
    window.location.reload();
  };

  public render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <div className="container">
                <p>이름</p>{" "}
                <input
                  onChange={this.handleChange}
                  name="inputname"
                  value={this.state.inputname}
                  type="text"
                  className="form-control"
                />
                <p className="mt-2">브랜드</p>
                <input
                  onChange={this.handleChange}
                  name="inputbrand"
                  value={this.state.inputbrand}
                  type="text"
                  className="form-control"
                />
                <p className="mt-2">
                  발매일
                  {timeFormat2(
                    new Date(
                      parseInt(this.state.inputyear),
                      parseInt(this.state.inputmonth) - 1,
                      parseInt(this.state.inputdate)
                    ).getTime()
                  )}{" "}
                </p>
                <div className="row">
                  <div className="col-4">
                    <p>년</p>
                    <input
                      onChange={this.handleChange}
                      name="inputyear"
                      value={this.state.inputyear}
                      type="text"
                      className="form-control"
                    />
                  </div>
                  <div className="col-4">
                    <p>월</p>
                    <input
                      onChange={this.handleChange}
                      name="inputmonth"
                      value={this.state.inputmonth}
                      type="text"
                      className="form-control"
                    />
                  </div>
                  <div className="col-4">
                    <p>일</p>
                    <input
                      onChange={this.handleChange}
                      name="inputdate"
                      value={this.state.inputdate}
                      type="text"
                      className="form-control"
                    />
                  </div>
                </div>
                <p className="mt-3">image</p>
                <img src={this.state.fileUrl} className="img-fluid" />
                <input
                  ref={this.fileRef}
                  type="file"
                  name="file"
                  onChange={this.handleFile}
                  className="d-none"
                />
                <button
                  onClick={this.handleFileRef}
                  className="btn btn-sm btn-success mt-1"
                >
                  이미지선택
                </button>
                <hr />
              </div>
            </div>
            <div className="w-100 text-center">
              <button
                onClick={this.handleSubmit}
                className="btn btn-lg btn-danger mt-1"
              >
                업로드
              </button>
            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default ReleaseAdd;
