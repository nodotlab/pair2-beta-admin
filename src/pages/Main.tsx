import React from "react";

// import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Main extends React.Component<any> {
    constructor(props: any) {
        super(props);
        this.state = {};
    }

    public componentDidMount() {
    }

    public render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                        <div
                            className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                            <h4>Dashboard</h4>
                            <div className="btn-toolbar mb-2 mb-md-0">
                                <div className="btn-group mr-2">
                                    <button className="btn btn-sm btn-dark">Share</button>
                                    <button className="btn btn-sm btn-dark">Export</button>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default Main;
