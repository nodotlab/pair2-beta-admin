import React from "react";
import { addFaq } from "../database/Actions";


class Faq {
  public title: string;
  public category: string;
  public desc: string;

  constructor(title: string, category: string, desc: string) {
    this.title = title;
    this.category = category;
    this.desc = desc;
  }
}

interface IFaq {
  title: string;
  category: string;
  desc: string;
}

class FaqPage extends React.Component<any, IFaq> {
  constructor(props: any) {
    super(props);
    this.state = {
      title: "",
      category: "",
      desc: ""
    };

  }

  public componentDidMount() {

  }

  private handleChange = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  }

  private handleSubmit = async () => {
    await addFaq({
      title: this.state.title,
      category: this.state.category,
      desc: this.state.desc,
    });
    alert("업로드완료");
    window.location.reload();
  }

  public render() {
    return (
      <div className="container-fluid">
      <div className="row">

      <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h4>FAQ</h4>
      </div>

      <p>카테고리 (텍스트, 중복 카테고리가 있으면 자동 분류 인식)</p>
      <input onChange={this.handleChange} name="category" value={this.state.category} className="form-control" />
      <p>제목</p>
      <input onChange={this.handleChange} name="title" value={this.state.title} className="form-control" />
      <p>내용</p>
      <textarea onChange={this.handleChange} name="desc" value={this.state.desc} className="form-control mb-1" rows={3} />
      <button onClick={this.handleSubmit} className="btn btn-success btn-sm float-right">추가</button>

      <h5 className="mt-5">FAQ List</h5>
      <hr className="mb-5" />

      </main>
      </div>
      </div>
    );
  }
}


export default FaqPage;