import React from "react";
import { timeFormat2, timeFormat5, currency } from "../Method";
import {
    addRelease,
    editRelease,
    uploadImage,
    getRelease,
    deleteRelease,
    deleteStore,
    putStore,
    getStore,
    editStore
} from "../database/Actions";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import ReleaseAdd from "./ReleaseAdd";
import moment from "moment";

interface IReleaseEdit {
    data: Release[];
    store: Store[];
    file: any;
    fileUrl: string;
    selectIndex: number;
    modal: boolean;
    modalStore: boolean;
    inputname: string;
    inputnameKr: string;
    inputbrand: string;
    inputprice: any;
    inputcolor: string;
    inputserial: string;
    inputreleaseDate: number;
    // inputyear: string;
    // inputmonth: string;
    inputdate: string;
    inputUrl: string;
    inputUrlLarge: string;
    body: string;
    unknownRelease: boolean;
    marketPriceUrlKream: string,
    marketPriceUrlStockx: string,
    editindex: number;
    editname: string;
    editnameKr: string;
    editbrand: string;
    editprice: any;
    editcolor: string;
    editserial: string;
    editreleaseDate: number;
    // edityear: string;
    // editmonth: string;
    editdate: string;
    editimageUrl: string;
    editimageUrlLarge: string;
    editbody: string;
    editunknownRelease: boolean;
    editMarketPriceUrlKream: string;
    editMarketPriceUrlStockx: string;
    storeindex: number;
    storename: string;
    storesaleType: string;
    storereleaseDate: number;
    storereleaseDateEnd: number;
    storeDate: string;
    storeTime: string;
    storeEndDate: string;
    storeEndTime: string;
    storeCountry: string;
    storereleaseType: string;
    storeprice: any;
    storeonline: string;
    storeurl: string;
    uploadMode: boolean;
    uploadModeStore: boolean;
    editModeStore: boolean;
    storeimageUrl: string;
}

interface Release {
    index: number;
    brand: string;
    imageUrl: string;
    imageUrlLarge: string;
    name: string;
    nameKr: string;
    unkownRelease: boolean;
    color: string;
    serial: string;
    releaseDate: number;
    type: number;
    unknownRelease: boolean;
    body: string;
    price: any;
    marketPriceUrlKream: string;
    marketPriceUrlStockx: string;
}

interface Store {
    storeUrl: string;
    index: number;
    imageUrl: string;
    release: number;
    online: boolean;
    releaseDate: number;
    releaseDateEnd: number;
    storename: string;
    storesaleType: string;
    storeCountry: string;
    storereleaseType: string;
    price: any;
}


class ReleaseEdit extends React.Component<any, IReleaseEdit> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            store: [],
            file: undefined,
            fileUrl: "",
            selectIndex: -1,
            modal: false,
            modalStore: false,
            inputname: "",
            inputnameKr: "",
            inputbrand: "",
            inputprice: "",
            inputcolor: "",
            inputserial: "",
            inputreleaseDate: 0,
            // inputyear: "",
            // inputmonth: "",
            inputdate: "",
            inputUrl: "",
            inputUrlLarge: "",
            marketPriceUrlKream: "",
            marketPriceUrlStockx: "",
            editindex: 0,
            editname: "",
            editnameKr: "",
            editbrand: "",
            editprice: "",
            editcolor: "",
            editserial: "",
            editreleaseDate: 0,
            // edityear: "",
            // editmonth: "",
            editdate: "",
            editimageUrl: "",
            editbody: "",
            editMarketPriceUrlKream: "",
            editMarketPriceUrlStockx: "",
            unknownRelease: false,
            editunknownRelease: false,
            editimageUrlLarge: "",
            body: "",
            storeindex: 0,
            storename: "",
            storesaleType: "추첨",
            storereleaseDate: 0,
            storereleaseDateEnd: 0,
            storeDate: "",
            storeTime: "",
            storeEndDate: "",
            storeEndTime: "",
            storeCountry: "국내",
            storereleaseType: "",
            storeprice: "",
            storeonline: "true",
            storeurl: "",
            uploadMode: false,
            uploadModeStore: false,
            editModeStore: false,
            storeimageUrl: ""
        };
    }

    public componentDidMount() {
        this.readRelease();
    }

    private fileRef: any = React.createRef();
    private handleFileRef = (): void => this.fileRef.current.click();

    private async readRelease() {
        const releases: Release[] = await getRelease();
        const arr: Release[] = [];
        for (let i in releases) {
            arr.push(releases[i]);
        }

        arr.sort((a: Release, b: Release) => {
            if (a.releaseDate > b.releaseDate) {
                return -1;
            } else {
                return 1;
            }
        });

        this.setState({data: arr});
    }

    private handleChange = (e: any): void => {
        this.setState({[e.target.name]: e.target.value} as any);
    };

    private handleChangeBody = (value: any): void =>
        this.setState({body: value} as any);

    private handleChagneBodyEdit = (value: any): void =>
        this.setState({editbody: value} as any);

    private handleFile = (e: any): void => {
        this.setState({
            file: e.target.files[0],
            fileUrl: URL.createObjectURL(e.target.files[0])
        });
    };

    private async imageUpload(file: any, size: string) {
        const formData = new FormData();
        formData.append("photos", file);
        const response = await uploadImage(formData, size);
        return response.result.Location;
    }

    private handleSubmit = async () => {
        const inputUrl = await this.imageUpload(this.state.file, "350");
        const inputUrlLarge = await this.imageUpload(this.state.file, "730");

        if (this.state.fileUrl !== "") {
            this.setState({
                inputUrl: inputUrl,
                inputUrlLarge: inputUrlLarge
            });
        }

        const data = {
            brand: this.state.inputbrand,
            name: this.state.inputname,
            nameKr: this.state.inputnameKr,
            price: this.state.inputprice,
            color: this.state.inputcolor,
            serial: this.state.inputserial,
            unknownRelease: this.state.unknownRelease,
            releaseDate: new Date(
                this.state.inputdate
            ).getTime(),
            imageUrl: inputUrl,
            imageUrlLarge: inputUrlLarge,
            body: this.state.body,
            marketPriceUrlKream: this.state.marketPriceUrlKream,
            marketPriceUrlStockx: this.state.marketPriceUrlStockx,

        };

        await addRelease(data);
        this.setState(
            {
                uploadMode: false,
                file: undefined,
                fileUrl: ""
            },
            () => alert("업로드되었습니다.")
        );
        this.readRelease();
    };


    //스토어 등록
    private handleSubmitStore = async () => {
        if (this.state.fileUrl !== "") {
            this.setState({
                inputUrl: await this.imageUpload(this.state.file, "150")
            });
        }

        const data = {
            release: this.state.data[this.state.selectIndex].index,
            storename: this.state.storename,
            storesaleType: this.state.storesaleType,
            storeCountry: this.state.storeCountry,
            releaseDate: new Date(
                this.state.storeDate + ' ' + this.state.storeTime
            ).getTime(),
            releaseDateEnd: new Date(
                this.state.storeEndDate + ' ' + this.state.storeEndTime
            ).getTime(),
            imageUrl: this.state.inputUrl ? this.state.inputUrl : "",
            price: this.state.storeprice,
            storeurl: this.state.storeurl,
            online: (this.state.storeonline === 'true')
        };

        await putStore(data);
        this.setState({
            uploadModeStore: false,
            storename: "",
            storesaleType: "추첨",
            storeCountry: "국내",
            storereleaseDate: 0,
            storereleaseDateEnd: 0,
            storeprice: "",
            storeonline: "true",
            storeurl: "",
            file: undefined,
            fileUrl: ""
        });

        const {result} = await getStore(
            this.state.data[this.state.selectIndex].index
        );
        this.setState({store: result});
    };

    private handleEdit = (index: number): void => {
        this.setState({
            modal: true,
            file: undefined,
            fileUrl: "",
            selectIndex: index,
            editindex: this.state.data[index].index,
            editname: this.state.data[index].name,
            editbrand: this.state.data[index].brand,
            // edityear: new Date(this.state.data[index].releaseDate)
            //   .getFullYear()
            //   .toString(),
            // editmonth: (
            //   new Date(this.state.data[index].releaseDate).getMonth() + 1
            // ).toString(),
            editdate: moment.unix(this.state.data[index].releaseDate / 1000).format("YYYY-MM-DD"),
            editprice: this.state.data[index].price
                ? this.state.data[index].price
                : 0,
            editreleaseDate: this.state.data[index].releaseDate,
            editimageUrl: this.state.data[index].imageUrl,
            editimageUrlLarge: this.state.data[index].imageUrlLarge
                ? this.state.data[index].imageUrlLarge
                : "",
            editnameKr: this.state.data[index].nameKr
                ? this.state.data[index].nameKr
                : "",
            editbody: this.state.data[index].body ? this.state.data[index].body : "",
            editunknownRelease: this.state.data[index].unknownRelease
                ? this.state.data[index].unknownRelease
                : false,
            editcolor: this.state.data[index].color
                ? this.state.data[index].color
                : "",
            editserial: this.state.data[index].serial
                ? this.state.data[index].serial
                : "",
            editMarketPriceUrlKream: this.state.data[index].marketPriceUrlKream
                ? this.state.data[index].marketPriceUrlKream
                : "",
            editMarketPriceUrlStockx: this.state.data[index].marketPriceUrlStockx
                ? this.state.data[index].marketPriceUrlStockx
                : ""
        });
    };

    private handleSubmitEdit = async () => {
        let inputUrl: string = this.state.editimageUrl;
        let inputUrlLarge: string = this.state.editimageUrlLarge;
        if (this.state.fileUrl !== "") {
            inputUrl = await this.imageUpload(this.state.file, "350");
            inputUrlLarge = await this.imageUpload(this.state.file, "730");
        }

        const data = {
            index: this.state.editindex,
            brand: this.state.editbrand,
            name: this.state.editname,
            nameKr: this.state.editnameKr,
            price: this.state.editprice,
            color: this.state.editcolor,
            serial: this.state.editserial,
            releaseDate: new Date(
                this.state.editdate
            ).getTime(),
            unknownRelease: this.state.editunknownRelease,
            imageUrl: inputUrl,
            imageUrlLarge: inputUrlLarge,
            marketPriceUrlKream: this.state.editMarketPriceUrlKream,
            marketPriceUrlStockx: this.state.editMarketPriceUrlStockx,
            body: this.state.editbody
        };

        await editRelease(data);
        this.setState({
            modal: false,
            editindex: 0,
            editname: "",
            editbrand: "",
            editreleaseDate: 0,
            // edityear: "",
            // editmonth: "",
            editdate: "",
            editimageUrl: "",
            body: "",
            editbody: "",
            file: undefined,
            fileUrl: "",
            editMarketPriceUrlKream: "",
            editMarketPriceUrlStockx: ""
        });

        this.readRelease();
    };

    private handleEditStore = (index: number): void => {
        this.setState({
            uploadModeStore: false,
            editModeStore: true,
            storeindex: this.state.store[index].index,
            storename: this.state.store[index].storename,
            storesaleType: this.state.store[index].storesaleType,
            storeCountry: this.state.store[index].storeCountry,
            storeDate: moment.unix(this.state.store[index].releaseDate / 1000).format("YYYY-MM-DD"),
            storeTime: moment.unix(this.state.store[index].releaseDate / 1000).format("HH:mm"),
            storeEndDate: moment.unix(this.state.store[index].releaseDateEnd / 1000).format("YYYY-MM-DD"),
            storeEndTime: moment.unix(this.state.store[index].releaseDateEnd / 1000).format("HH:mm"),
            storeprice: this.state.store[index].price.toString(),
            storeurl: this.state.store[index].storeUrl,
            storeonline: String(this.state.store[index].online),
            storeimageUrl: this.state.store[index].imageUrl,
            fileUrl: ""
        });
    };

    private handleSubmitEditStore = async () => {
        let imageUrl = "";
        if (this.state.fileUrl !== "") {
            imageUrl = await this.imageUpload(this.state.file, "150");
            this.setState({
                inputUrl: imageUrl
            });
        }

        const data = {
            index: this.state.storeindex,
            release: this.state.data[this.state.selectIndex].index,
            storename: this.state.storename,
            storesaleType: this.state.storesaleType,
            storeCountry: this.state.storeCountry,
            releaseDate: new Date(
                this.state.storeDate + ' ' + this.state.storeTime
            ).getTime(),
            releaseDateEnd: new Date(
                this.state.storeEndDate + ' ' + this.state.storeEndTime
            ).getTime(),
            imageUrl: imageUrl === "" ? this.state.storeimageUrl : imageUrl,
            price: this.state.storeprice,
            storeurl: this.state.storeurl,
            online: (this.state.storeonline === 'true')
        };

        await editStore(data);
        this.setState({
            uploadModeStore: false,
            storename: "",
            storesaleType: "추첨",
            storeCountry: "국내",
            storereleaseDate: 0,
            storereleaseDateEnd: 0,
            storeprice: "",
            storeonline: "true",
            storeurl: "",
            editModeStore: false,
            file: undefined,
            fileUrl: ""
        });

        const {result} = await getStore(
            this.state.data[this.state.selectIndex].index
        );

        alert("수정 되었습니다.");

        this.setState({store: result});
    };

    private handleStoreModal = async (index: number) => {
        this.setState({
            modalStore: true,
            uploadModeStore: true,
            editModeStore: false,
            selectIndex: index // select index arr index...
        });

        const {result} = await getStore(this.state.data[index].index);
        this.setState({
            store: result
        });
    };

    private handleDelete = async (index: number) => {
        if (window.confirm("스니커즈를 삭제 하시겠습니까?")) {
            await deleteRelease(this.state.data[index].index);
            alert("삭제 되었습니다.");
            this.readRelease();
        } else {
            return;
        }
    };

    private handleStoreDelete = async (index: number) => {
        if (window.confirm("스토어를 삭제 하시겠습니까?")) {
            await deleteStore(this.state.store[index].index);

            const {result} = await getStore(
                this.state.data[this.state.selectIndex].index
            );

            alert("삭제 되었습니다.");
            this.setState({store: result});
        } else {
            return;
        }
    };

    public render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                        <div
                            className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                            <div className="container">
                                {this.state.uploadMode && <this.ComponentUpload/>}
                                <hr/>

                                <div className="row">
                                    <div className="col-6">
                                        <h5 className="mt-5">스니커 리스트</h5>
                                        <p>
                                            이미지 마우스 올려놓으면 확대 | 업로드모드를 통해서 업로드
                                        </p>
                                    </div>
                                    <div className="col-6 text-right">
                                        <button
                                            onClick={() =>
                                                this.setState({uploadMode: !this.state.uploadMode})
                                            }
                                            className="btn btn-sm btn-primary mt-1"
                                        >
                                            캘린더 업로드모드
                                        </button>
                                    </div>
                                </div>
                                {this.state.data.map((data: Release, i: number) => (
                                    <div key={i}>
                                        <div className="row">
                                            <div className="col-2">
                                                <img className="img-thumb ml-5" src={data.imageUrl}/>
                                            </div>
                                            <div className="col-6 pt-2">
                                                <p>
                                                    <b>브랜드:</b> {data.brand}
                                                </p>
                                                <p>
                                                    <b>이름:</b> {data.name} | <b>한글:</b> {data.nameKr}
                                                </p>
                                                <p>
                                                    <b>color:</b> {data.color} | <b>serial:</b>{" "}
                                                    {data.serial}
                                                </p>
                                            </div>
                                            <div className="col-3 ">
                                                <p>
                                                    <b>가격:</b> &nbsp;{" "}
                                                    {data.price ? currency(data.price) : ""}
                                                </p>
                                                <p>
                                                    <b>월중표기:</b>{" "}
                                                    {data.unknownRelease ? "True" : "False"}
                                                </p>
                                                <p>
                                                    <b>발매일:</b> {timeFormat2(data.releaseDate)}
                                                </p>
                                            </div>
                                            <div className="col-1 text-right">
                                                <button
                                                    onClick={() => this.handleStoreModal(i)}
                                                    className="btn btn-primary btn-xs"
                                                >
                                                    스토어
                                                </button>
                                                <button
                                                    data-target="#"
                                                    onClick={() => this.handleEdit(i)}
                                                    className="btn btn-success btn-xs mt-1"
                                                    data-toggle="modal"
                                                >
                                                    수정
                                                </button>
                                                <br/>
                                                <button
                                                    onClick={() => this.handleDelete(i)}
                                                    className="btn btn-danger btn-xs mt-1"
                                                >
                                                    삭제
                                                </button>
                                            </div>
                                        </div>
                                        <hr className="mt-0"/>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </main>
                </div>

                {this.state.modal && <this.ComponentModalEdit/>}
                {this.state.modalStore && <this.ComponentModalStore/>}
            </div>
        );
    }

    private ComponentUpload = () => {
        return (
            <>
                <div className="row">
                    <div className="col-12 mb-3">
                        <p className="bold">영문명</p>
                        <input
                            onChange={this.handleChange}
                            name="inputname"
                            value={this.state.inputname}
                            type="text"
                            className="form-control"
                        />
                    </div>
                    <div className="col-12 mb-3">
                        <p className="bold">한글명</p>
                        <input
                            onChange={this.handleChange}
                            name="inputnameKr"
                            value={this.state.inputnameKr}
                            type="text"
                            className="form-control"
                        />
                    </div>
                    <div className="col-6 mb-3">
                        <p className="bold">브랜드</p>
                        <input
                            onChange={this.handleChange}
                            name="inputbrand"
                            value={this.state.inputbrand}
                            type="text"
                            className="form-control"
                        />
                    </div>
                    <div className="col-6 mb-3">
                        <p className="bold">가격 {currency(this.state.inputprice)}</p>
                        <input
                            onChange={this.handleChange}
                            // onChange={this.priceChange}
                            name="inputprice"
                            value={this.state.inputprice}
                            type="text"
                            className="form-control"
                        />
                    </div>
                    <div className="col-6 mb-3">
                        <p className="bold">색상</p>
                        <input
                            onChange={this.handleChange}
                            name="inputcolor"
                            value={this.state.inputcolor}
                            type="text"
                            className="form-control"
                        />
                    </div>
                    <div className="col-6 mb-3">
                        <p className="bold">품번</p>
                        <input
                            onChange={this.handleChange}
                            name="inputserial"
                            value={this.state.inputserial}
                            type="text"
                            className="form-control"
                        />
                    </div>
                    <div className="col-2  mt-2 mb-4">
                        <p className="bold">
                            발매일 확정여부
                        </p>
                    </div>
                    <div className="col-10 mt-2 mb-4">
                        {this.state.unknownRelease ? (
                            <button
                                onClick={() => this.setState({unknownRelease: false})}
                                className="btn btn-sm btn-primary"
                            >
                                발매일 미정
                            </button>
                        ) : (
                            <button
                                onClick={() => this.setState({unknownRelease: true})}
                                className="btn btn-sm btn-primary"
                            >
                                발매일 확정
                            </button>
                        )}
                    </div>
                    <div className="col-12">
                        <p className="bold">발매일</p>
                        <p className="p-11">
                            발매일이 미정인 경우 해당 루머의 월 말일을 입력합니다. 예) 8월중 발매 루머가 있는 경우 "2021년 8월 31일" 입력
                        </p>
                    </div>
                    {/* <div className="col-4 mb-3">
            <input
              onChange={this.handleChange}
              name="inputyear"
              value={this.state.inputyear}
              type="text"
              className="form-control"
              placeholder="년"
            />
          </div>
          <div className="col-4 mb-3">
            <input
              onChange={this.handleChange}
              name="inputmonth"
              value={this.state.inputmonth}
              type="text"
              className="form-control"
              placeholder="월"
            />
          </div> */}
                    <div className="col-4 mb-3">
                        <input
                            onChange={this.handleChange}
                            name="inputdate"
                            value={this.state.inputdate}
                            type="date"
                            className="form-control"
                        />
                    </div>
                    <div className="col-12">
                        <p className="bold">리셀마켓 시세정보</p>
                        <p className="p-11">
                            크림과 스탁엑스에서 해당 스니커즈 링크 입력
                        </p>
                    </div>
                    <div className="col-12 mb-3 row">
                        <div className="col-1">
                            <p className="mt-2 bold">KREAM</p>
                        </div>
                        <div className="col-11">
                            <input
                                onChange={this.handleChange}
                                name="marketPriceUrlKream"
                                value={this.state.marketPriceUrlKream}
                                type="text"
                                className="form-control"
                                placeholder="KREAM 링크"
                            />
                        </div>
                    </div>
                    <div className="col-12 mb-3 row">
                        <div className="col-1">
                            <p className="mt-2 bold">STOCKX</p>
                        </div>
                        <div className="col-11">
                            <input
                                onChange={this.handleChange}
                                name="marketPriceUrlStockx"
                                value={this.state.marketPriceUrlStockx}
                                type="text"
                                className="form-control"
                                placeholder="STOCKX 링크"
                            />
                        </div>
                    </div>
                    <div className="col-12 article-desc-box">
                        <p className="bold mt-3">
                            런칭캘린더 내용
                        </p>
                        <div>
                            <ReactQuill
                                className="w-100 h-500"
                                value={this.state.body}
                                onChange={this.handleChangeBody}
                            />
                        </div>
                    </div>
                    <div className="col-12">
                        <button
                            onClick={this.handleFileRef}
                            className="btn btn-sm btn-success mt-1"
                        >
                            이미지선택
                        </button>
                        <img src={this.state.fileUrl} className="img-fluid"/>
                        <input
                            ref={this.fileRef}
                            type="file"
                            name="file"
                            onChange={this.handleFile}
                            className="d-none"
                        />
                    </div>
                </div>


                <div className="w-100 text-right">
                    <button
                        onClick={this.handleSubmit}
                        className="btn btn-sm btn-danger mt-1"
                    >
                        업로드
                    </button>
                </div>
            </>
        );
    };

    private ComponentModalStore = () => {
        return (
            <div className="modal" role="dialog">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                {this.state.data[this.state.selectIndex].name} 스토어
                            </h5>
                            <button
                                onClick={() => this.setState({modalStore: false})}
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="text-right">
                                <button
                                    onClick={() =>
                                        this.setState({
                                            uploadModeStore: !this.state.uploadModeStore,
                                            editModeStore: false
                                        })
                                    }
                                    className="btn btn-sm btn-primary"
                                >
                                    스토어 등록
                                </button>
                            </div>

                            {this.state.uploadModeStore && (
                                <>
                                    <div className="row mt-3 mb-3">
                                        <div className="col-2">
                                            <p className="bold mt-2">스토어명</p>
                                        </div>
                                        <div className="col-10">
                                            <input
                                                onChange={this.handleChange}
                                                name="storename"
                                                value={this.state.storename}
                                                type="text"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>


                                    <div className="row mt-3">
                                        <div className="col-2">
                                            <p className="mt-2 bold">시작시간</p>
                                        </div>
                                        <div className="col-5 pr-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeDate"
                                                value={this.state.storeDate}
                                                type="date"
                                                className="form-control"
                                            />
                                        </div>
                                        <div className="col-5 pl-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeTime"
                                                value={this.state.storeTime}
                                                type="time"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="row mt-1 mb-3">
                                        <div className="col-2">
                                            <p className="mt-2 bold">종료시간</p>
                                        </div>
                                        <div className="col-5 pr-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeEndDate"
                                                value={this.state.storeEndDate}
                                                type="date"
                                                className="form-control"
                                            />
                                        </div>
                                        <div className="col-5 pl-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeEndTime"
                                                value={this.state.storeEndTime}
                                                type="time"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="row mt-3 mb-3">
                                        <div className="col-2">
                                            <p className="bold mt-2">가격</p>
                                        </div>
                                        <div className="col-10">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeprice"
                                                value={this.state.storeprice}
                                                type="text"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="row mt-2 mb-2">
                                        <div className="col-2">
                                            <p className="bold mt-2">응모링크</p>
                                        </div>
                                        <div className="col-10">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeurl"
                                                value={this.state.storeurl}
                                                type="text"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>


                                    <div className="row mt-3 mb-3">
                                        <div className="col-3">
                                            <p className="bold mt-1">발매유형</p>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeRaffle">
                                                <input type="radio" name="storesaleType" id="typeRaffle" value="추첨"
                                                       checked={this.state.storesaleType === "추첨"}
                                                       onChange={this.handleChange}/>&nbsp;추첨
                                            </label>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeFCFS">
                                                <input type="radio" name="storesaleType" id="typeFCFS" value="선착순"
                                                       checked={this.state.storesaleType === "선착순"}
                                                       onChange={this.handleChange}/>&nbsp;선착순
                                            </label>
                                        </div>
                                    </div>

                                    <div className="row mt-3 mb-3">
                                        <div className="col-3">
                                            <p className="bold mt-1">온/오프라인</p>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeOnline">
                                                <input type="radio" name="storeonline" id="typeOnline" value="true"
                                                       checked={this.state.storeonline === "true"}
                                                       onChange={this.handleChange}/>&nbsp;온라인
                                            </label>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeOffline">
                                                <input type="radio" name="storeonline" id="typeOffline" value="false"
                                                       checked={this.state.storeonline === "false"}
                                                       onChange={this.handleChange}/>&nbsp;오프라인
                                            </label>
                                        </div>
                                    </div>

                                    <div className="row mt-2 mb-2">
                                        <div className="col-3">
                                            <p className="bold mt-1">국내/해외</p>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeKorea">
                                                <input type="radio" name="storeCountry" id="typeKorea" value="국내"
                                                       checked={this.state.storeCountry === "국내"}
                                                       onChange={this.handleChange}/>&nbsp;국내
                                            </label>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeWorldwide">
                                                <input type="radio" name="storeCountry" id="typeWorldwide" value="해외"
                                                       checked={this.state.storeCountry === "해외"}
                                                       onChange={this.handleChange}/>&nbsp;해외
                                            </label>
                                        </div>
                                    </div>

                                    <div className="row mt-2 mb-2">
                                        <div className="col-3">
                                            <p className="mt-1 bold">스토어 이미지</p>
                                        </div>
                                        <div className="col-9">
                                            <button
                                                onClick={this.handleFileRef}
                                                className="btn btn-sm btn-success mt-1"
                                            >
                                                이미지 선택
                                            </button>
                                        </div>
                                    </div>
                                    <div className="w-100 text-right">
                                        <button
                                            onClick={this.handleSubmitStore}
                                            className="btn btn-sm btn-danger mt-1"
                                        >
                                            등록완료
                                        </button>
                                    </div>
                                    <img src={this.state.fileUrl} className="img-fluid"/>
                                    <input
                                        ref={this.fileRef}
                                        type="file"
                                        name="file"
                                        onChange={this.handleFile}
                                        className="d-none"
                                    />


                                </>
                            )}

                            {this.state.editModeStore && (
                                <>
                                    <div className="row mt-3 mb-3">
                                        <div className="col-2">
                                            <p className="bold mt-2">스토어명</p>
                                        </div>
                                        <div className="col-10">
                                            <input
                                                onChange={this.handleChange}
                                                name="storename"
                                                value={this.state.storename}
                                                type="text"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="row mt-3">
                                        <div className="col-2">
                                            <p className="mt-2 bold">시작시간</p>
                                        </div>
                                        <div className="col-5 pr-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeDate"
                                                value={this.state.storeDate}
                                                type="date"
                                                className="form-control"
                                            />
                                        </div>
                                        <div className="col-5 pl-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeTime"
                                                value={this.state.storeTime}
                                                type="time"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="row mt-1 mb-3">
                                        <div className="col-2">
                                            <p className="mt-2 bold">종료시간</p>
                                        </div>
                                        <div className="col-5 pr-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeEndDate"
                                                value={this.state.storeEndDate}
                                                type="date"
                                                className="form-control"
                                            />
                                        </div>
                                        <div className="col-5 pl-2">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeEndTime"
                                                value={this.state.storeEndTime}
                                                type="time"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="row mt-3 mb-3">
                                        <div className="col-2">
                                            <p className="bold mt-2">가격</p>
                                        </div>
                                        <div className="col-10">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeprice"
                                                value={this.state.storeprice}
                                                type="text"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>


                                    <div className="row mt-2 mb-2">
                                        <div className="col-2">
                                            <p className="bold mt-2">응모링크</p>
                                        </div>
                                        <div className="col-10">
                                            <input
                                                onChange={this.handleChange}
                                                name="storeurl"
                                                value={this.state.storeurl}
                                                type="text"
                                                className="form-control"
                                            />
                                        </div>
                                    </div>

                                    <div className="row mt-3 mb-3">
                                        <div className="col-3">
                                            <p className="bold mt-1">발매유형</p>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeRaffle">
                                                <input type="radio" name="storesaleType" id="typeRaffle" value="추첨"
                                                       checked={this.state.storesaleType === "추첨"}
                                                       onChange={this.handleChange}/>&nbsp;추첨
                                            </label>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeFCFS">
                                                <input type="radio" name="storesaleType" id="typeFCFS" value="선착순"
                                                       checked={this.state.storesaleType === "선착순"}
                                                       onChange={this.handleChange}/>&nbsp;선착순
                                            </label>
                                        </div>
                                    </div>

                                    <div className="row mt-3 mb-3">
                                        <div className="col-3">
                                            <p className="bold mt-1">온/오프라인</p>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeOnline">
                                                <input type="radio" name="storeonline" id="typeOnline" value="true"
                                                       checked={this.state.storeonline === "true"}
                                                       onChange={this.handleChange}/>&nbsp;온라인
                                            </label>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="typeOffline">
                                                <input type="radio" name="storeonline" id="typeOffline" value="false"
                                                       checked={this.state.storeonline === "false"}
                                                       onChange={this.handleChange}/>&nbsp;오프라인
                                            </label>
                                        </div>
                                    </div>

                                    <div className="row mt-2 mb-2">
                                        <div className="col-3">
                                            <p className="bold mt-1">국내/해외</p>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="korea">
                                                <input type="radio" name="storeCountry" id="korea" value="국내"
                                                       checked={this.state.storeCountry === "국내"}
                                                       onChange={this.handleChange}/>&nbsp;국내
                                            </label>
                                        </div>
                                        <div className="col-2">
                                            <label htmlFor="worldwide">
                                                <input type="radio" name="storeCountry" id="worldwide" value="해외"
                                                       checked={this.state.storeCountry === "해외"}
                                                       onChange={this.handleChange}/>&nbsp;해외
                                            </label>
                                        </div>
                                    </div>

                                    <div className="row mt-2 mb-2">
                                        <div className="col-3">
                                            <p className="mt-1 bold">스토어 이미지</p>
                                            <p className="p-11">(이미지 변경 시에만 사용)</p>
                                        </div>
                                        <div className="col-9">
                                            <button
                                                onClick={this.handleFileRef}
                                                className="btn btn-sm btn-success mt-1"
                                            >
                                                이미지 선택
                                            </button>
                                        </div>
                                    </div>
                                    <div className="w-100 text-right">
                                        <button
                                            onClick={this.handleSubmitEditStore}
                                            className="btn btn-sm btn-danger mt-1"
                                        >
                                            수정완료
                                        </button>
                                    </div>
                                    <img src={this.state.fileUrl} className="img-fluid"/>
                                    <input
                                        ref={this.fileRef}
                                        type="file"
                                        name="file"
                                        onChange={this.handleFile}
                                        className="d-none"
                                    />


                                </>
                            )}

                            {this.state.store.map((data: Store, i: number) => (
                                <div className="row pt-2 mt-2 boder-top-line" key={i}>
                                    <div className="col-3">
                                        <img className="img-fluid" src={data.imageUrl}/>
                                    </div>
                                    <div className="col-7">
                                        <p>스토어명: {data.storename}</p>
                                        <p>시작시간: {timeFormat5(data.releaseDate)}</p>
                                        <p>종료시간: {timeFormat5(data.releaseDateEnd)}</p>
                                        <p>가격: {currency(data.price)}</p>
                                        <p>발매유형: {data.storesaleType}</p>
                                        <p>온/오프라인: {data.online ? "온라인" : "오프라인"}</p>
                                        <p>국내/해외: {data.storeCountry}</p>
                                        <p>
                                            응모링크:{" "}
                                            <a href={data.storeUrl} target="_new">
                                                {"링크확인"}
                                            </a>
                                        </p>
                                    </div>
                                    <div className="col-2 pt-2">
                                        <button
                                            onClick={() =>
                                                this.handleEditStore(i)
                                            }
                                            className="btn btn-sm btn-success"
                                        >
                                            수정
                                        </button>
                                        <br/>
                                        <button
                                            onClick={() => this.handleStoreDelete(i)}
                                            className="btn btn-danger btn-sm mt-1"
                                        >
                                            삭제
                                        </button>
                                    </div>
                                    <hr/>
                                </div>
                            ))}

                            {/* 업로드모드 종료 */}
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    private ComponentModalEdit = () => {
        return (
            <div className="modal" id="exampleModal" role="dialog">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">
                                {this.state.data[this.state.selectIndex].brand} {this.state.data[this.state.selectIndex].name} 수정
                            </h5>
                            <button
                                onClick={() => this.setState({modal: false})}
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <img
                                className="img-fluid"
                                src={this.state.data[this.state.selectIndex].imageUrl}
                            />
                            <p>
                                <b>영문명 :</b> {this.state.editname}
                            </p>
                            <p>
                                <b>한글명 :</b> {this.state.editnameKr}
                            </p>
                            <p>
                                <b>브랜드 :</b> {this.state.editbrand}
                            </p>
                            <p>
                                <b>가격 :</b> {currency(this.state.editprice)}
                            </p>
                            <p>
                                <b>색상 :</b> {this.state.editcolor}
                            </p>
                            <p>
                                <b>품번 :</b> {this.state.editserial}
                            </p>
                            <p>
                                <b>발매일 확정 여부:</b>{" "}
                                {this.state.editunknownRelease ? "발매일 미정" : "발매일 확정"}
                            </p>
                            <p>
                                <b>발매일 :</b>{" "}
                                {timeFormat2(
                                    this.state.data[this.state.selectIndex].releaseDate
                                )}
                            </p>

                            <hr/>
                            <div className="row">
                                <div className="col-12 mb-3">
                                    <p className="bold">영문명</p>
                                    <input
                                        onChange={this.handleChange}
                                        name="editname"
                                        value={this.state.editname}
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-12 mb-3">
                                    <p className="bold">한글명</p>
                                    <input
                                        onChange={this.handleChange}
                                        name="editnameKr"
                                        value={this.state.editnameKr}
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <p className="mt-2 bold">브랜드</p>
                                    <input
                                        onChange={this.handleChange}
                                        name="editbrand"
                                        value={this.state.editbrand}
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <p className="mt-2 bold">가격</p>
                                    <input
                                        onChange={this.handleChange}
                                        name="editprice"
                                        value={this.state.editprice}
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <p className="mt-2 bold">색상</p>
                                    <input
                                        onChange={this.handleChange}
                                        name="editcolor"
                                        value={this.state.editcolor}
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-6 mb-3">
                                    <p className="mt-2 bold">품번</p>
                                    <input
                                        onChange={this.handleChange}
                                        name="editserial"
                                        value={this.state.editserial}
                                        type="text"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-2 mt-2 mb-4">
                                    <p className="bold">
                                        발매일 확정여부
                                    </p>
                                </div>
                                <div className="col-10 mt-2 mb-4">
                                    {this.state.editunknownRelease ? (
                                        <button
                                            onClick={() => this.setState({editunknownRelease: false})}
                                            className="btn btn-sm btn-primary"
                                        >
                                            발매일 미정
                                        </button>
                                    ) : (
                                        <button
                                            onClick={() => this.setState({editunknownRelease: true})}
                                            className="btn btn-sm btn-primary"
                                        >
                                            발매일 확정
                                        </button>
                                    )}
                                </div>
                                <div className="col-12">
                                    <p className="bold">발매일</p>
                                    <p className="p-11">
                                        발매일이 미정인 경우 해당 루머의 월 말일을 입력합니다. 예) 8월중 발매 루머가 있는 경우 "2021년 8월 31일" 입력
                                    </p>
                                </div>
                                {/* <div className="col-4 mb-3">
                  <input
                    onChange={this.handleChange}
                    name="edityear"
                    value={this.state.edityear}
                    type="text"
                    className="form-control"
                    placeholder="년"
                  />
                </div>
                <div className="col-4">
                  <input
                    onChange={this.handleChange}
                    name="editmonth"
                    value={this.state.editmonth}
                    type="text"
                    className="form-control"
                    placeholder="월"
                  />
                </div> */}
                                <div className="col-4">
                                    <input
                                        onChange={this.handleChange}
                                        name="editdate"
                                        value={this.state.editdate}
                                        type="date"
                                        className="form-control"
                                    />
                                </div>
                                <div className="col-12">
                                    <p className="bold">리셀마켓 시세정보</p>
                                    <p className="p-11">
                                        크림과 스탁엑스에서 해당 스니커즈 링크 입력
                                    </p>
                                </div>
                                <div className="col-12 mb-3 row">
                                    <div className="col-1">
                                        <p className="mt-2 bold">KREAM</p>
                                    </div>
                                    <div className="col-11">
                                        <input
                                            onChange={this.handleChange}
                                            name="editMarketPriceUrlKream"
                                            value={this.state.editMarketPriceUrlKream}
                                            type="text"
                                            className="form-control"
                                            placeholder="KREAM 링크"
                                        />
                                    </div>
                                </div>
                                <div className="col-12 mb-3 row">
                                    <div className="col-1">
                                        <p className="mt-2 bold">STOCKX</p>
                                    </div>
                                    <div className="col-11">
                                        <input
                                            onChange={this.handleChange}
                                            name="editMarketPriceUrlStockx"
                                            value={this.state.editMarketPriceUrlStockx}
                                            type="text"
                                            className="form-control"
                                            placeholder="STOCKX 링크"
                                        />
                                    </div>
                                </div>
                                <div className="col-12 article-desc-box">
                                    <p className="bold mt-3">
                                        런칭캘린더 내용
                                    </p>
                                    <div className="mt-2 mb-3">
                                        <ReactQuill
                                            className="w-100 h-500"
                                            value={this.state.editbody}
                                            onChange={this.handleChagneBodyEdit}
                                        />
                                    </div>
                                </div>
                                <div className="col-12">
                                    <img src={this.state.fileUrl} className="img-fluid"/>
                                    <input
                                        ref={this.fileRef}
                                        type="file"
                                        name="file"
                                        onChange={this.handleFile}
                                        className="d-none"
                                    />
                                    <button
                                        onClick={this.handleFileRef}
                                        className="btn btn-sm btn-success mt-1"
                                    >
                                        이미지선택
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button
                                onClick={this.handleSubmitEdit}
                                type="button"
                                className="btn btn-primary"
                            >
                                수정확정
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

export default ReleaseEdit;
