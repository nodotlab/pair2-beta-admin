import React from "react";
import { getCommision, putCommision } from "../database/Actions";

interface ICommision {
  getCommision: number;
  commision: number;
  getSale: number;
  sale: number;
}

class Commision extends React.Component<any, ICommision> {
  constructor(props: any) {
    super(props);
    this.state = {
      getCommision: 0,
      commision: 0,
      getSale: 0,
      sale: 0
    };

    this.readCommision();
  }

  public componentDidMount() {}

  private async readCommision() {
    const result = await getCommision();
    console.log(result);

    this.setState({
      getCommision: result.commision,
      commision: result.commision,
      getSale: result.sale,
      sale: result.sale
    });
  }

  private handleChange = (e: any): void => {
    console.log(parseFloat(e.target.value));
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handleSubmit = async () => {
    await putCommision(this.state.commision, this.state.sale);
    this.readCommision();
  };

  public render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h4>Commision</h4>
            </div>

            <h5>수수료설정</h5>
            <p>
              현재 수수료:{" "}
              {this.state.getCommision !== 0 && this.state.getCommision + "%"}
            </p>
            <p>수정될 수수료: {this.state.commision + "%"}</p>
            <input
              onChange={this.handleChange}
              className="form-control"
              name="commision"
              value={this.state.commision}
              type="text"
            />

            <h5>할인 설정</h5>
            <p>
              현재 할인: {this.state.getSale !== 0 && this.state.getSale + "%"}
            </p>
            <p>수정될 할인: {this.state.sale + "%"}</p>
            <input
              onChange={this.handleChange}
              className="form-control"
              name="sale"
              value={this.state.sale}
              type="text"
            />

            <button
              onClick={this.handleSubmit}
              className="btn btn-success mt-3"
            >
              수정
            </button>
          </main>
        </div>
      </div>
    );
  }
}

export default Commision;
