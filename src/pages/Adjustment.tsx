import React from "react";
import {
  getAdjustment,
  updateAdjustment,
  getCommision
} from "../database/Actions";
import { currency } from "../Method";

interface Adjustment {
  index: number;
  sellerIndex: number;
  nickname: string;
  name: string;
  phone: string;
  bank: string;
  account: string;
  owner: string;
  price: any;
  tradeIndex: number;
  tradeName: string;
  tradeSize: string;
  smsAgree: boolean;
  complete: boolean;
}

interface IAdjustment {
  data: Adjustment[];
  commision: number;
  sale: number;
}

class AdjustmentClass extends React.Component<any, IAdjustment> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: [],
      commision: 0,
      sale: 0
    };
  }

  public componentDidMount() {
    this.getData();
    this.getCommision();
  }

  private async getCommision() {
    const result: any = await getCommision();
    console.log(result);
    this.setState({
      commision: result.commision,
      sale: result.sale
    });
  }

  private async getData() {
    let result = await getAdjustment();
    console.log(result);
    result = result.result;
    const arr: Adjustment[] = [];

    for (const i in result) {
      arr.push(result[i]);
    }
    this.setState({ data: arr });
  }

  private handleSubmitComplete = async (
    tradeIndex: number,
    adjustmentIndex: number
  ) => {
    await updateAdjustment(tradeIndex, adjustmentIndex);
    this.getData();
  };

  public render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div className="mt-3">
              <div className="w-100 d-flex">
                {/* <p className="w-769 text-center bold">판매자인덱스</p>
        <p className="w-769 text-center bold">닉네임</p>
        <p className="w-769 text-center bold">이름</p>
        <p className="w-769 text-center bold">전화번호</p>
        <p className="w-769 text-center bold">은행명</p>
        <p className="w-769 text-center bold">계좌번호</p>
        <p className="w-769 text-center bold">예금주</p>
        <p className="w-769 text-center bold">정산금액</p>
        <p className="w-769 text-center bold">거래번호</p>
        <p className="w-769 text-center bold">제품명</p>
        <p className="w-769 text-center bold">사이즈</p>
        <p className="w-769 text-center bold">문자수신동의</p>
        <p className="w-769 text-center bold">정산전/정산후</p> */}

                <p className="w-14 text-center bold">거래번호</p>
                <p className="w-14 text-center bold">제품명</p>
                <p className="w-14 text-center bold">사이즈</p>
                <p className="w-14 text-center bold">판매자인덱스</p>
                <p className="w-14 text-center bold">닉네임</p>
                <p className="w-14 text-center bold">이름</p>
                <p className="w-14 text-center bold">전화번호</p>
                <p className="w-14 text-center bold">은행명</p>
                <p className="w-14 text-center bold">계좌번호</p>
                <p className="w-14 text-center bold">예금주</p>
                <p className="w-14 text-center bold">판매금액</p>
                <p className="w-14 text-center bold">정산예정금액</p>
                <p className="w-14 text-center bold">문자수신동의</p>
                <p className="w-14 text-center bold">정산여부</p>
              </div>
              {this.state.data.map((data: Adjustment, i: number) => (
                <>
                  <hr />
                  <div key={i} className="w-100 d-flex">
                    <p className="w-14 text-center">{data.tradeIndex}</p>
                    <p className="w-14 text-center">{data.tradeName}</p>
                    <p className="w-14 text-center">{data.tradeSize}</p>
                    <p className="w-14 text-center">{data.sellerIndex}</p>
                    <p className="w-14 text-center">{data.nickname}</p>
                    <p className="w-14 text-center">{data.name}</p>
                    <p className="w-14 text-center">{data.phone}</p>
                    <p className="w-14 text-center">{data.bank}</p>
                    <p className="w-14 text-center">{data.account}</p>
                    <p className="w-14 text-center">{data.owner}</p>
                    <p className="w-14 text-center">{currency(data.price)}</p>
                    <p className="w-14 text-center">
                      {/* {currency(
                        // Math.round(
                        //   data.price -
                        //     (data.price * this.state.commision * 0.01 -
                        //       data.price * this.state.sale * 0.01)
                        // )
                      )} */}
                      {" "}
                      <br />
                      수수료: {this.state.commision}%<br />
                      할인: {this.state.sale}%
                    </p>
                    <p className="w-14 text-center">
                      {data.smsAgree ? "동의" : "비동의"}
                    </p>
                    {!data.complete ? (
                      <button
                        onClick={() =>
                          this.handleSubmitComplete(data.tradeIndex, data.index)
                        }
                        className="w-14 btn btn-sm btn-primary"
                      >
                        정산전
                      </button>
                    ) : (
                      <button className="w-14 btn btn-sm btn-success">
                        정산완료
                      </button>
                    )}
                  </div>
                </>
              ))}
            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default AdjustmentClass;
