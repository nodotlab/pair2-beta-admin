import React from "react";
import { timeFormat2 } from "../Method";
import {
  getSneakers,
  searchSneakers,
  uploadImage,
  editSneaker,
  deleteSneaker
} from "../database/Actions";
import SneakerAdd from "./SneakersAdd";

class Sneaker {
  public index: number;
  public name: string;
  public name2: string;
  public brand: string;
  public serial: string;
  public releaseDate: number;
  public price: number;
  public thumbUrl: string;
  public imageUrl1: string;
  public imageUrl2: string;
  public imageUrl3: string;
  public imageUrl4: string;
  public imageUrl5: string;
  public dollar: boolean;

  constructor(
    _index: number,
    name: string,
    name2: string,
    brand: string,
    serial: string,
    _releaseDate: number,
    price: number,
    _thumbUrl: string,
    imageUrl1: string,
    imageUrl2: string,
    imageUrl3: string,
    imageUrl4: string,
    imageUrl5: string,
    _dollar: boolean
  ) {
    this.index = _index;
    this.name = name;
    this.name2 = name2;
    this.brand = brand;
    this.serial = serial;
    this.releaseDate = _releaseDate;
    this.price = price;
    this.thumbUrl = _thumbUrl;
    this.imageUrl1 = imageUrl1;
    this.imageUrl2 = imageUrl2;
    this.imageUrl3 = imageUrl3;
    this.imageUrl4 = imageUrl4;
    this.imageUrl5 = imageUrl5;
    this.dollar = _dollar;
  }
}

interface ISneakerEdit {
  data: Sneaker[];
  file: any;
  file1: any;
  file2: any;
  file3: any;
  file4: any;
  file5: any;
  fileUrl: string;
  fileUrl1: string;
  fileUrl2: string;
  fileUrl3: string;
  fileUrl4: string;
  fileUrl5: string;
  selectIndex: number;
  modal: boolean;

  inputindex: number;
  inputname: string;
  inputname2: string;
  inputbrand: string;
  inputserial: string;
  inputreleaseDate: number;
  inputprice: number;
  inputyear: string;
  inputmonth: string;
  inputdate: string;
  inputthumbUrl: string;
  inputimageUrl1: string;
  inputimageUrl2: string;
  inputimageUrl3: string;
  inputimageUrl4: string;
  inputimageUrl5: string;
  inputdollar: boolean;
  sneakerAdd: boolean;
}

class SneakerEdit extends React.Component<any, ISneakerEdit> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: [],
      file: undefined,
      file1: undefined,
      file2: undefined,
      file3: undefined,
      file4: undefined,
      file5: undefined,
      fileUrl: "",
      fileUrl1: "",
      fileUrl2: "",
      fileUrl3: "",
      fileUrl4: "",
      fileUrl5: "",
      selectIndex: -1,
      modal: false,

      inputindex: 0,
      inputname: "",
      inputname2: "",
      inputbrand: "",
      inputserial: "",
      inputreleaseDate: 0,
      inputyear: "",
      inputmonth: "",
      inputdate: "",
      inputprice: 0,
      inputthumbUrl: "",
      inputimageUrl1: "",
      inputimageUrl2: "",
      inputimageUrl3: "",
      inputimageUrl4: "",
      inputimageUrl5: "",
      inputdollar: false,
      sneakerAdd: false
    };
  }

  public componentDidMount() {
    this.readQuery();
  }

  private fileRef: any = React.createRef();
  private handleFileRef = (): void => this.fileRef.current.click();
  private fileRef1: any = React.createRef();
  private handleFileRef1 = (): void => this.fileRef1.current.click();
  private fileRef2: any = React.createRef();
  private handleFileRef2 = (): void => this.fileRef2.current.click();
  private fileRef3: any = React.createRef();
  private handleFileRef3 = (): void => this.fileRef3.current.click();
  private fileRef4: any = React.createRef();
  private handleFileRef4 = (): void => this.fileRef4.current.click();
  private fileRef5: any = React.createRef();
  private handleFileRef5 = (): void => this.fileRef5.current.click();

  private handleFile = (e: any): void => {
    this.setState({
      file: e.target.files[0],
      fileUrl: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile1 = (e: any): void => {
    this.setState({
      file1: e.target.files[0],
      fileUrl1: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile2 = (e: any): void => {
    this.setState({
      file2: e.target.files[0],
      fileUrl2: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile3 = (e: any): void => {
    this.setState({
      file3: e.target.files[0],
      fileUrl3: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile4 = (e: any): void => {
    this.setState({
      file4: e.target.files[0],
      fileUrl4: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile5 = (e: any): void => {
    this.setState({
      file5: e.target.files[0],
      fileUrl5: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleChange = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private readQuery(): void {
    if (this.props.match.params.search === undefined) {
      this.getSneakers("full");
    } else {
      this.getSneakers("search");
      // console.log(this.props.match.params.search);
    }
  }

  private async getSneakers(type: string) {
    const sneakers: Sneaker[] = [];
    let data = undefined;

    if (type === "full") {
      data = await getSneakers();
    } else {
      data = await searchSneakers(this.props.match.params.search);
      // console.log(this.props.match.params.search);
    }

    for (let i = 0; i < data.result.length; ++i) {
      const each: Sneaker = new Sneaker(
        data.result[i].index,
        data.result[i].name,
        data.result[i].nameKr,
        data.result[i].brand,
        data.result[i].serial,
        data.result[i].releaseDate,
        data.result[i].releasePrice,
        data.result[i].thumbUrl ? data.result[i].thumbUrl : "",
        data.result[i].imageUrl1 ? data.result[i].imageUrl1 : "",
        data.result[i].imageUrl2 ? data.result[i].imageUrl2 : "",
        data.result[i].imageUrl3 ? data.result[i].imageUrl3 : "",
        data.result[i].imageUrl4 ? data.result[i].imageUrl4 : "",
        data.result[i].imageUrl5 ? data.result[i].imageUrl5 : "",
        data.result[i].dollar
      );

      sneakers.push(each);
    }

    this.setState({ data: sneakers });
    // console.log(this.state.data);
  }

  private async imageUpload(file: any, size: string) {
    const formData = new FormData();
    formData.append("photos", file);

    const response = await uploadImage(formData, size);
    return response.result.Location;
  }

  private handleModal = (): void => this.setState({ modal: !this.state.modal });

  private handleEdit = (index: number): void => {
    this.setState({
      selectIndex: index,
      inputindex: this.state.data[index].index,
      inputname: this.state.data[index].name,
      inputname2: this.state.data[index].name2,
      inputbrand: this.state.data[index].brand,
      inputreleaseDate: this.state.data[index].releaseDate,
      inputyear: new Date(this.state.data[index].releaseDate)
        .getFullYear()
        .toString(),
      inputmonth: (
        new Date(this.state.data[index].releaseDate).getMonth() + 1
      ).toString(),
      inputdate: new Date(this.state.data[index].releaseDate)
        .getDate()
        .toString(),
      inputprice: this.state.data[index].price,
      inputserial: this.state.data[index].serial,
      inputdollar: this.state.data[index].dollar,
      inputthumbUrl: this.state.data[index].thumbUrl,
      inputimageUrl1: this.state.data[index].imageUrl1,
      inputimageUrl2: this.state.data[index].imageUrl2,
      inputimageUrl3: this.state.data[index].imageUrl3,
      inputimageUrl4: this.state.data[index].imageUrl4,
      inputimageUrl5: this.state.data[index].imageUrl5,

      file: undefined,
      file1: undefined,
      file2: undefined,
      file3: undefined,
      file4: undefined,
      file5: undefined,
      fileUrl: "",
      fileUrl1: "",
      fileUrl2: "",
      fileUrl3: "",
      fileUrl4: "",
      fileUrl5: ""
    });

    this.handleModal();
  };

  private handleSubmit = async () => {
    if (this.state.fileUrl !== "") {
      this.setState({
        inputthumbUrl: await this.imageUpload(this.state.file, "350")
      });
    }

    if (this.state.fileUrl1 !== "") {
      this.setState({
        inputimageUrl1: await this.imageUpload(this.state.file1, "730")
      });
    }

    if (this.state.fileUrl2 !== "") {
      this.setState({
        inputimageUrl2: await this.imageUpload(this.state.file2, "730")
      });
    }

    if (this.state.fileUrl3 !== "") {
      this.setState({
        inputimageUrl3: await this.imageUpload(this.state.file3, "730")
      });
    }

    if (this.state.fileUrl4 !== "") {
      this.setState({
        inputimageUrl4: await this.imageUpload(this.state.file4, "730")
      });
    }

    if (this.state.fileUrl5 !== "") {
      this.setState({
        inputimageUrl5: await this.imageUpload(this.state.file5, "730")
      });
    }

    const result = await editSneaker({
      index: this.state.inputindex,
      name: this.state.inputname,
      nameKr: this.state.inputname2,
      brand: this.state.inputbrand,
      dollar: this.state.inputdollar,
      releaseDate: new Date(
        parseInt(this.state.inputyear),
        parseInt(this.state.inputmonth) - 1,
        parseInt(this.state.inputdate)
      ).getTime(),
      releasePrice: this.state.inputprice,
      serial: this.state.inputserial,
      thumbUrl: this.state.inputthumbUrl,
      imageUrl1: this.state.inputimageUrl1,
      imageUrl2: this.state.inputimageUrl2,
      imageUrl3: this.state.inputimageUrl3,
      imageUrl4: this.state.inputimageUrl4,
      imageUrl5: this.state.inputimageUrl5
    });
    this.setState({ selectIndex: -1 });

    this.handleModal();
    this.readQuery();
  }; // edit submit

  private handleDelete = (i: number) => {
    deleteSneaker(i);
    this.readQuery();
  };

  public render() {
    return (
      <div className="container-fluid">
        {this.state.sneakerAdd && <SneakerAdd />}

        <div className="row">
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h4>스니커</h4>
              <button
                className="btn btn-success mt-1"
                onClick={() =>
                  this.setState({ sneakerAdd: !this.state.sneakerAdd })
                }
              >
                업로드모드
              </button>
            </div>
            <h5 className="mt-5">스니커 리스트</h5>
            <p>
              이미지 마우스 올려놓으면 확대 | 상단 네비바를 이용하여 이름으로
              검색
            </p>

            {this.state.data.map((sneaker: Sneaker, i: number) => (
              <div key={i}>
                <div className="row">
                  <div className="col-2">
                    <img className="img-thumb ml-5" src={sneaker.thumbUrl} />
                    <img className="img-thumb ml-5" src={sneaker.imageUrl1} />
                    <img className="img-thumb ml-5" src={sneaker.imageUrl2} />
                    <img className="img-thumb ml-5" src={sneaker.imageUrl3} />
                    <img className="img-thumb ml-5" src={sneaker.imageUrl4} />
                    <img className="img-thumb ml-5" src={sneaker.imageUrl5} />
                  </div>
                  <div className="col-2 pt-2">
                    <p>
                      <b>이름:</b> {sneaker.name}
                    </p>
                    <p>
                      <b>한글이름:</b> {sneaker.name2}
                    </p>
                    <p>
                      <b>발매가:</b> {sneaker.price}{" "}
                      {sneaker.dollar ? "달러" : "원"}
                    </p>
                    <p>
                      <b>품번:</b> {sneaker.serial}
                    </p>
                    <p>
                      <b>발매일:</b> {timeFormat2(sneaker.releaseDate)}
                    </p>
                  </div>
                  <div className="col-6">
                    <p>
                      <b>이미지 url:</b> {sneaker.thumbUrl}
                    </p>
                    <p>
                      <b>이미지 url1:</b> {sneaker.imageUrl1}
                    </p>
                    <p>
                      <b>이미지 url2:</b> {sneaker.imageUrl2}
                    </p>
                    <p>
                      <b>이미지 url3:</b> {sneaker.imageUrl3}
                    </p>
                    <p>
                      <b>이미지 url4:</b> {sneaker.imageUrl4}
                    </p>
                    <p>
                      <b>이미지 url5:</b> {sneaker.imageUrl5}
                    </p>
                  </div>
                  <div className="col-1 pt-3 text-center">
                    <h6>{sneaker.brand}</h6>
                  </div>
                  <div className="col-1 pt-3">
                    <button
                      onClick={() => this.handleEdit(i)}
                      className="btn btn-success btn-xs"
                    >
                      수정
                    </button>
                    <br />
                    <button
                      onClick={() => this.handleDelete(sneaker.index)}
                      className="btn btn-danger btn-xs mt-3"
                    >
                      삭제
                    </button>
                  </div>
                </div>
                <hr className="mt-0" />
              </div>
            ))}
          </main>
        </div>

        {this.state.selectIndex > -1 && <this.ComponentModal />}
      </div>
    );
  }

  private ComponentModal = () => {
    return (
      <div
        className="modal"
        style={{ display: this.state.modal ? "block" : "none" }}
        role="dialog"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                {this.state.data[this.state.selectIndex].name} 을 수정합니다.
              </h5>
              <button
                onClick={this.handleModal}
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>이름</p>
              <input
                onChange={this.handleChange}
                name="inputname"
                value={this.state.inputname}
                type="text"
                className="form-control"
              />
              <p className="mt-2">한글이름</p>
              <input
                onChange={this.handleChange}
                name="inputname2"
                value={this.state.inputname2}
                type="text"
                className="form-control"
              />
              <p className="mt-2">브랜드</p>
              <input
                onChange={this.handleChange}
                name="inputbrand"
                value={this.state.inputbrand}
                type="text"
                className="form-control"
              />
              <p className="mt-2">발매가</p>
              <input
                onChange={this.handleChange}
                name="inputprice"
                value={this.state.inputprice}
                type="text"
                className="form-control"
              />
              <p className="mt-2">품번</p>
              <input
                onChange={this.handleChange}
                name="inputserial"
                value={this.state.inputserial}
                type="text"
                className="form-control"
              />

              <p className="mt-2">
                발매일 {timeFormat2(this.state.inputreleaseDate)} 수정후{" "}
                {timeFormat2(
                  new Date(
                    parseInt(this.state.inputyear),
                    parseInt(this.state.inputmonth) - 1,
                    parseInt(this.state.inputdate)
                  ).getTime()
                )}{" "}
              </p>
              <div className="row">
                <div className="col-4">
                  <p>년</p>
                  <input
                    onChange={this.handleChange}
                    name="inputyear"
                    value={this.state.inputyear}
                    type="text"
                    className="form-control"
                  />
                </div>
                <div className="col-4">
                  <p>월</p>
                  <input
                    onChange={this.handleChange}
                    name="inputmonth"
                    value={this.state.inputmonth}
                    type="text"
                    className="form-control"
                  />
                </div>
                <div className="col-4">
                  <p>일</p>
                  <input
                    onChange={this.handleChange}
                    name="inputdate"
                    value={this.state.inputdate}
                    type="text"
                    className="form-control"
                  />
                </div>
              </div>

              <p className="mt-3">Thumbnail image</p>
              <p>기존</p>
              <img src={this.state.inputthumbUrl} className="img-fluid" />
              <p>수정이미지</p>
              <img src={this.state.fileUrl} className="img-fluid" />
              <input
                ref={this.fileRef}
                type="file"
                name="file"
                onChange={this.handleFile}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image1</p>
              <p>기존</p>
              <img src={this.state.inputimageUrl1} className="img-fluid" />
              <p>수정이미지</p>
              <img src={this.state.fileUrl1} className="img-fluid" />
              <input
                ref={this.fileRef1}
                type="file"
                name="file"
                onChange={this.handleFile1}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef1}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image2</p>
              <p>기존</p>
              <img src={this.state.inputimageUrl2} className="img-fluid" />
              <p>수정이미지</p>
              <img src={this.state.fileUrl2} className="img-fluid" />
              <input
                ref={this.fileRef2}
                type="file"
                name="file"
                onChange={this.handleFile2}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef2}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image3</p>
              <p>기존</p>
              <img src={this.state.inputimageUrl3} className="img-fluid" />
              <p>수정이미지</p>
              <img src={this.state.fileUrl3} className="img-fluid" />
              <input
                ref={this.fileRef3}
                type="file"
                name="file"
                onChange={this.handleFile3}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef3}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image4</p>
              <p>기존</p>
              <img src={this.state.inputimageUrl4} className="img-fluid" />
              <p>수정이미지</p>
              <img src={this.state.fileUrl4} className="img-fluid" />
              <input
                ref={this.fileRef4}
                type="file"
                name="file"
                onChange={this.handleFile4}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef4}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image5</p>
              <p>기존</p>
              <img src={this.state.inputimageUrl5} className="img-fluid" />
              <p>수정이미지</p>
              <img src={this.state.fileUrl5} className="img-fluid" />
              <input
                ref={this.fileRef5}
                type="file"
                name="file"
                onChange={this.handleFile5}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef5}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
            </div>
            <div className="modal-footer">
              <button
                onClick={this.handleModal}
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                onClick={this.handleSubmit}
                type="button"
                className="btn btn-primary"
              >
                수정확정
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default SneakerEdit;
