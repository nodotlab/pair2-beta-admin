import React from "react";
import { getTrade } from "../database/Actions";

interface ITrade {
    data: any;
}

class TradeClass extends React.Component<any, ITrade> {
    constructor(props: any) {
        super(props);
        this.state = {
            data: []
        };
    }

    public componentDidMount() {
        this.getData();
    }

    private async getData() {
        const arr: any = [];
        const result = await getTrade();

        for (let i in result) {
            arr.push(result[i]);
        }

        this.setState({data: arr});
    }

    public render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                        <div className="mt-3">
                            <div className="w-100 d-flex">
                                <p className="w-18p text-center bold">매물사진</p>
                                <p className="w-18p text-center bold">매물인덱스</p>
                                <p className="w-18p text-center bold">제품명</p>
                                <p className="w-18p text-center bold">사이즈</p>
                                <p className="w-18p text-center bold">가격</p>
                                <p className="w-18p text-center bold">판매자</p>
                                <p className="w-18p text-center bold">
                                    판매자
                                    <br/>
                                    연락처
                                </p>
                                <p className="w-18p text-center bold">거래상태</p>
                                <p className="w-18p text-center bold">거래수단</p>
                                <p className="w-18p text-center bold">결제금액</p>
                                <p className="w-18p text-center bold">결제일자</p>
                                <p className="w-18p text-center bold">구매자</p>
                                <p className="w-18p text-center bold">
                                    구매자
                                    <br/>
                                    연락처
                                </p>
                                <p className="w-18p text-center bold">
                                    거래파기
                                    <br/>
                                    신고여부
                                </p>
                                <p className="w-18p text-center bold">
                                    거래파기
                                    <br/>
                                    신고일자
                                </p>
                                <p className="w-18p text-center bold">
                                    거래파기
                                    <br/>
                                    신고자
                                </p>
                                <p className="w-18p text-center bold">
                                    거래파기
                                    <br/>
                                    사유
                                </p>
                                <p className="w-18p text-center bold">
                                    거래파기
                                    <br/>
                                    상세사유
                                </p>
                            </div>
                            {this.state.data.map((data: any, i: number) => (
                                <>
                                    <hr/>
                                    <div key={i} className="w-100 d-flex">
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">{data.index}</p>
                                        <p className="w-18p text-center">{data.sneaker.name}</p>
                                        <p className="w-18p text-center">{data.size}</p>
                                        <p className="w-18p text-center">{data.price}</p>
                                        <p className="w-18p text-center">
                                            {data.seller.nickname}/{data.seller.name}
                                        </p>
                                        <p className="w-18p text-center">{data.seller.phone}</p>
                                        <p className="w-18p text-center">{data.status}</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">{}</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">-</p>
                                        <p className="w-18p text-center">-</p>
                                    </div>
                                </>
                            ))}
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default TradeClass;
