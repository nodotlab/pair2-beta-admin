import React from "react";
import { Consumer, ContextInterface as Ictx, ctxt } from "../Store";

class Signin extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  static contextType = ctxt;

  private handleChange = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handleSignin = (): void => {
    this.context.signin(this.state.email, this.state.password);
  };

  public render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h4>SignIn</h4>
            </div>
            <this.ComponentSignin />
          </main>
        </div>
      </div>
    );
  }

  private ComponentSignin = () => {
    return (
      <div className="col-md-4 offset-md-4">
        <h5>Email</h5>
        <input
          onChange={this.handleChange}
          className="form-control mb-2"
          type="email"
          name="email"
          value={this.state.email}
        />
        <h5>Password</h5>
        <input
          onChange={this.handleChange}
          className="form-control"
          type="password"
          name="password"
          value={this.state.password}
        />

        <button
          onClick={this.handleSignin}
          className="btn btn-success float-right mt-3"
        >
          로그인
        </button>
      </div>
    );
  };
}

export default Signin;
