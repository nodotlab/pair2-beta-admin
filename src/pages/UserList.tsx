import React from "react";
import { getTrade, getUserList } from "../database/Actions";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

interface IUser {
    data: any;
    defaultColDef: any;
    columnDefs: Array<ColumnDefs>;
    gridItem: any;
}

interface ColumnDefs {
    headerName: string;
    field: string;
}

class UserListClass extends React.Component<any, IUser> {
    private gridApi: any;
    private columnApi: any;

    constructor(props: any) {
        super(props);
        this.state = {
            data: [],
            defaultColDef: {
                sortable: true,
                filter: "agTextColumnFilter",
                floatingFilter: true,
                resizable: true,
                autoSize: true,
                autoSizeColumns: true,
            },
            columnDefs: [
                {headerName: "이름", field: "name"},
                {headerName: "닉네임", field: "nickname"},
                {headerName: "전화번호", field: "phone"},
                {headerName: "이메일", field: "email"},
                {headerName: "사이즈", field: "size"},
                {headerName: "나이", field: "age"},
                {headerName: "성별", field: "sex"},
            ],
            gridItem: [],
        };
    }

    public componentDidMount() {
        this.getData();
    }

    public autoSizeColumns(params: any) {
        const colIds = params.columnApi
            .getAllDisplayedColumns()
            .map((col: { getColId: () => any; }) => col.getColId());

        params.columnApi.autoSizeColumns(colIds);
    }

    private async getData() {
        const arr: any = [];
        const result = await getUserList();

        this.setState({gridItem: result.userList});
    }

    onGridReady(params: any) {
        this.gridApi = params.api;
        this.columnApi = params.columnApi;
        params.api.sizeColumnsToFit();
    }

    public render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                        <div style={{paddingTop: 20, height: "100%"}}>
                            <div
                                className="ag-theme-alpine"
                                style={{height: "800px", width: "100%"}}>
                                <AgGridReact
                                    pagination={true}
                                    defaultColDef={this.state.defaultColDef}
                                    columnDefs={this.state.columnDefs}
                                    rowData={this.state.gridItem}
                                    onFirstDataRendered={this.onGridReady}
                                />
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default UserListClass;