import React from "react";
import { getFilters, putFilter, deleteFilter } from "../database/Actions";

class Filter {
  public index: number;
  public name: string;
  public other: boolean;

  constructor(index: number, name: string, other: boolean) {
    this.index = index;
    this.name = name;
    this.other = other;
  }
}

interface IFilters {
  brand: string;
  other: boolean;
  filters: Filter[];
  selectFilter: number;
}

class Filters extends React.Component<any, IFilters> {
  constructor(props: any) {
    super(props);
    this.state = {
      brand: "",
      other: false,
      filters: [],
      selectFilter: -1
    };
  }

  public componentDidMount() {
    this.getFilters();
  }

  private async getFilters() {
    const data = await getFilters();
    this.setState({ filters: data.result });
    console.log(this.state.filters);
  }

  private handleChange = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private handleDelete = async () => {
    if (
      window.confirm(
        `${this.state.filters[this.state.selectFilter].name} 를 삭제합니까?`
      )
    ) {
      await deleteFilter(this.state.filters[this.state.selectFilter].index);
      this.getFilters();
      this.setState({ selectFilter: -1 });
    }
  };

  private handleSubmit = async () => {
    if (this.state.brand === "") return;

    await putFilter(this.state.brand, this.state.other);
    this.getFilters();
    this.setState({ brand: "" });
  };

  public render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
              <h4>브랜드 필터 추가</h4>
            </div>

            <div className="row">
              <div className="col-md-6">
                <h5 className="mb-3">
                  현재 브랜드 필터의 모습 (삭제를 원할시 클릭)
                </h5>
                <b>- 일반브랜드</b>
                {this.state.filters.map(
                  (filter: Filter, i: number) =>
                    !filter.other && (
                      <p
                        onClick={() => this.setState({ selectFilter: i })}
                        key={i}
                        className="pointer"
                      >
                        {filter.name}
                      </p>
                    )
                )}

                <br />
                <b>- 기타브랜드</b>
                {this.state.filters.map(
                  (filter: Filter, i: number) =>
                    filter.other && (
                      <p
                        onClick={() => this.setState({ selectFilter: i })}
                        key={i}
                        className="pointer"
                      >
                        {filter.name}
                      </p>
                    )
                )}
              </div>

              <div className="col-md-6">
                {this.state.selectFilter > -1 && (
                  <div>
                    <h5>삭제</h5>
                    <p>{this.state.filters[this.state.selectFilter].name}</p>
                    <button
                      onClick={this.handleDelete}
                      className="btn btn-sm btn-danger mt-1"
                    >
                      삭제
                    </button>
                  </div>
                )}
              </div>
            </div>

            <hr />

            <div className="col-md-4 offset-md-4">
              <h5>브랜드 필터 추가</h5>
              <p>브랜드명</p>
              <input
                onChange={this.handleChange}
                className="form-control"
                value={this.state.brand}
                name="brand"
              />
              <p className="mt-3">일반 혹은 기타브랜드 (클릭)</p>
              {this.state.other ? (
                <button
                  onClick={() => this.setState({ other: !this.state.other })}
                  className="btn btn-sm btn-success"
                >
                  기타브랜드
                </button>
              ) : (
                <button
                  onClick={() => this.setState({ other: !this.state.other })}
                  className="btn btn-sm btn-success"
                >
                  일반브랜드
                </button>
              )}

              <hr />
              <button
                onClick={this.handleSubmit}
                className="btn btn-sm btn-success mt-1 float-right"
              >
                추가
              </button>
            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default Filters;
