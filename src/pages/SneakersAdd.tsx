import React from "react";
import { array, currency, timeFormat2 } from "../Method";
import { addSneaker, editSneaker, uploadImage } from "../database/Actions";

interface ISneaker {
  file: any;
  file1: any;
  file2: any;
  file3: any;
  file4: any;
  file5: any;
  fileUrl: string;
  fileUrl1: string;
  fileUrl2: string;
  fileUrl3: string;
  fileUrl4: string;
  fileUrl5: string;
  selectIndex: number;
  modal: boolean;

  inputindex: number;
  inputname: string;
  inputname2: string;
  inputbrand: string;
  inputserial: string;
  inputreleaseDate: number;
  inputprice: number;
  inputyear: string;
  inputmonth: string;
  inputdate: string;
  inputthumbUrl: string;
  inputimageUrl1: string;
  inputimageUrl2: string;
  inputimageUrl3: string;
  inputimageUrl4: string;
  inputimageUrl5: string;
  inputdollar: boolean;
}

class Sneakers extends React.Component<any, ISneaker> {
  constructor(props: any) {
    super(props);
    this.state = {
      file: undefined,
      file1: undefined,
      file2: undefined,
      file3: undefined,
      file4: undefined,
      file5: undefined,
      fileUrl: "",
      fileUrl1: "",
      fileUrl2: "",
      fileUrl3: "",
      fileUrl4: "",
      fileUrl5: "",
      selectIndex: -1,
      modal: false,

      inputindex: 0,
      inputname: "",
      inputname2: "",
      inputbrand: "",
      inputserial: "",
      inputreleaseDate: 0,
      inputyear: "",
      inputmonth: "",
      inputdate: "",
      inputprice: 0,
      inputthumbUrl: "",
      inputimageUrl1: "",
      inputimageUrl2: "",
      inputimageUrl3: "",
      inputimageUrl4: "",
      inputimageUrl5: "",
      inputdollar: false
    };
  }

  private handleChange = (e: any): void => {
    this.setState({ [e.target.name]: e.target.value } as any);
  };

  private fileRef: any = React.createRef();
  private handleFileRef = (): void => this.fileRef.current.click();
  private fileRef1: any = React.createRef();
  private handleFileRef1 = (): void => this.fileRef1.current.click();
  private fileRef2: any = React.createRef();
  private handleFileRef2 = (): void => this.fileRef2.current.click();
  private fileRef3: any = React.createRef();
  private handleFileRef3 = (): void => this.fileRef3.current.click();
  private fileRef4: any = React.createRef();
  private handleFileRef4 = (): void => this.fileRef4.current.click();
  private fileRef5: any = React.createRef();
  private handleFileRef5 = (): void => this.fileRef5.current.click();

  private handleFile = (e: any): void => {
    this.setState({
      file: e.target.files[0],
      fileUrl: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile1 = (e: any): void => {
    this.setState({
      file1: e.target.files[0],
      fileUrl1: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile2 = (e: any): void => {
    this.setState({
      file2: e.target.files[0],
      fileUrl2: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile3 = (e: any): void => {
    this.setState({
      file3: e.target.files[0],
      fileUrl3: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile4 = (e: any): void => {
    this.setState({
      file4: e.target.files[0],
      fileUrl4: URL.createObjectURL(e.target.files[0])
    });
  };

  private handleFile5 = (e: any): void => {
    this.setState({
      file5: e.target.files[0],
      fileUrl5: URL.createObjectURL(e.target.files[0])
    });
  };

  private async imageUpload(file: any, size: string) {
    const formData = new FormData();
    formData.append("photos", file);

    const response = await uploadImage(formData, size);
    return response.result.Location;
  }

  private handleSubmit = async () => {
    if (this.state.fileUrl !== "") {
      this.setState({
        inputthumbUrl: await this.imageUpload(this.state.file, "350")
      });
    }

    if (this.state.fileUrl1 !== "") {
      this.setState({
        inputimageUrl1: await this.imageUpload(this.state.file1, "730")
      });
    }

    if (this.state.fileUrl2 !== "") {
      this.setState({
        inputimageUrl2: await this.imageUpload(this.state.file2, "730")
      });
    }

    if (this.state.fileUrl3 !== "") {
      this.setState({
        inputimageUrl3: await this.imageUpload(this.state.file3, "730")
      });
    }

    if (this.state.fileUrl4 !== "") {
      this.setState({
        inputimageUrl4: await this.imageUpload(this.state.file4, "730")
      });
    }

    if (this.state.fileUrl5 !== "") {
      this.setState({
        inputimageUrl5: await this.imageUpload(this.state.file5, "730")
      });
    }

    const data = {
      brand: this.state.inputbrand,
      dollar: this.state.inputdollar,
      name: this.state.inputname,
      nameKr: this.state.inputname2,
      releaseDate: new Date(
        parseInt(this.state.inputyear),
        parseInt(this.state.inputmonth) - 1,
        parseInt(this.state.inputdate)
      ).getTime(),
      releasePrice: this.state.inputprice,
      serial: this.state.inputserial,
      thumbUrl: this.state.inputthumbUrl ? this.state.inputthumbUrl : false,
      imageUrl1: this.state.inputimageUrl1 ? this.state.inputimageUrl1 : false,
      imageUrl2: this.state.inputimageUrl2 ? this.state.inputimageUrl2 : false,
      imageUrl3: this.state.inputimageUrl3 ? this.state.inputimageUrl3 : false,
      imageUrl4: this.state.inputimageUrl4 ? this.state.inputimageUrl4 : false,
      imageUrl5: this.state.inputimageUrl5 ? this.state.inputimageUrl5 : false
    };

    await addSneaker(data);
    alert("업로드완료");
    window.location.reload();
  };

  public render() {
    return (
      <div className="col-md-6 offset-md-3">
        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
          <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <div className="container">
              <p>이름</p>
              <input
                onChange={this.handleChange}
                name="inputname"
                value={this.state.inputname}
                type="text"
                className="form-control"
              />
              <p className="mt-2">한글이름</p>
              <input
                onChange={this.handleChange}
                name="inputname2"
                value={this.state.inputname2}
                type="text"
                className="form-control"
              />
              <p className="mt-2">브랜드</p>
              <input
                onChange={this.handleChange}
                name="inputbrand"
                value={this.state.inputbrand}
                type="text"
                className="form-control"
              />
              <p className="mt-2">
                발매가 기준 {this.state.inputdollar ? "달러" : "원화"}
              </p>
              <p onClick={() => this.setState({ inputdollar: true })}>
                발매가기준 선택  달러
              </p>
              <p onClick={() => this.setState({ inputdollar: false })}>
                발매가기준 선택  원화
              </p>
              <input
                onChange={this.handleChange}
                name="inputprice"
                value={this.state.inputprice}
                type="text"
                className="form-control"
              />
              <p className="mt-2">품번</p>
              <input
                onChange={this.handleChange}
                name="inputserial"
                value={this.state.inputserial}
                type="text"
                className="form-control"
              />

              <p className="mt-2">
                발매일
                {timeFormat2(
                  new Date(
                    parseInt(this.state.inputyear),
                    parseInt(this.state.inputmonth) - 1,
                    parseInt(this.state.inputdate)
                  ).getTime()
                )}{" "}
              </p>
              <div className="row">
                <div className="col-4">
                  <p>년</p>
                  <input
                    onChange={this.handleChange}
                    name="inputyear"
                    value={this.state.inputyear}
                    type="text"
                    className="form-control"
                  />
                </div>
                <div className="col-4">
                  <p>월</p>
                  <input
                    onChange={this.handleChange}
                    name="inputmonth"
                    value={this.state.inputmonth}
                    type="text"
                    className="form-control"
                  />
                </div>
                <div className="col-4">
                  <p>일</p>
                  <input
                    onChange={this.handleChange}
                    name="inputdate"
                    value={this.state.inputdate}
                    type="text"
                    className="form-control"
                  />
                </div>
              </div>

              <p className="mt-3">Thumbnail image</p>
              <img src={this.state.fileUrl} className="img-fluid" />
              <input
                ref={this.fileRef}
                type="file"
                name="file"
                onChange={this.handleFile}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image1</p>
              <img src={this.state.fileUrl1} className="img-fluid" />
              <input
                ref={this.fileRef1}
                type="file"
                name="file"
                onChange={this.handleFile1}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef1}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image2</p>
              <img src={this.state.fileUrl2} className="img-fluid" />
              <input
                ref={this.fileRef2}
                type="file"
                name="file"
                onChange={this.handleFile2}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef2}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image3</p>
              <img src={this.state.fileUrl3} className="img-fluid" />
              <input
                ref={this.fileRef3}
                type="file"
                name="file"
                onChange={this.handleFile3}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef3}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image4</p>
              <img src={this.state.fileUrl4} className="img-fluid" />
              <input
                ref={this.fileRef4}
                type="file"
                name="file"
                onChange={this.handleFile4}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef4}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
              <hr />

              <p>image5</p>
              <img src={this.state.fileUrl5} className="img-fluid" />
              <input
                ref={this.fileRef5}
                type="file"
                name="file"
                onChange={this.handleFile5}
                className="d-none"
              />
              <button
                onClick={this.handleFileRef5}
                className="btn btn-sm btn-success mt-1"
              >
                이미지선택
              </button>
            </div>
          </div>
          <div className="w-100 text-center">
            <button onClick={this.handleSubmit} className="btn btn-danger mt-1">
              업로드
            </button>
          </div>
        </main>
      </div>
    );
  }
}

export default Sneakers;
