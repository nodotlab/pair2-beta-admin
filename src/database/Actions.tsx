import { hostUrl } from "../reduce/Urls";
import axios from "axios";

const crypto = require("crypto");

export const getSneakers = async () => {
    const reulst: any = await axios.get(`${hostUrl}/admin/data/sneakers`);
    return reulst.data;
};

export const getAdjustment = async () => {
    const reulst: any = await axios.get(`${hostUrl}/admin/data/adjustment`);
    return reulst.data;
};

export const updateAdjustment = async (
    tradeIndex: number,
    adjustmentIndex: number
) => {
    const reulst: any = await axios.get(
        `${hostUrl}/admin/update/adjustment?tradeIndex=${tradeIndex}&adjustmentIndex=${adjustmentIndex}`
    );
    return reulst.data;
};

export const searchSneakers = async (search: string) => {
    const result = await axios.get(`${hostUrl}/search/sneakers?search=${search}`);
    return result.data;
};

export const getFilters = async () => {
    const reulst: any = await axios.get(`${hostUrl}/data/filters`);
    return reulst.data;
};

export const putFilter = async (name: string, other: boolean) => {
    const reulst: any = await axios.post(`${hostUrl}/admin/put/filter`, {
        name: name,
        other: other
    });
    return reulst.data;
};

export const deleteFilter = async (index: number) => {
    const reulst: any = await axios.get(
        `${hostUrl}/admin/delete/filter?index=${index}`
    );
    return reulst.data;
};

export const uploadImage = async (formData: any, size: string) => {
    const config = {
        headers: {
            Accept: "application/json",
            size: size
        }
    };

    const result = await axios.post(
        `${hostUrl}/admin/upload/image`,
        formData,
        config
    );
    return result.data;
};

export const uploadImageRelease = async (formData: any, size: string) => {
    const config = {
        headers: {
            Accept: "application/json",
            size: size
        }
    };

    const result = await axios.post(
        `${hostUrl}/admin/upload/imagerelease`,
        formData,
        config
    );
    return result.data;
};

export const getRelease = async () => {
    const result: any = await axios.get(`${hostUrl}/admin/data/release`);
    return result.data.result;
};

export const addSneaker = async (data: any) => {
    const result: any = await axios.post(`${hostUrl}/admin/add/sneaker`, data);
    return result.data;
};

export const addRelease = async (data: any) => {
    const result: any = await axios.post(`${hostUrl}/admin/add/release`, data);
    return result.data;
};

export const editRelease = async (data: any) => {
    const result: any = await axios.post(`${hostUrl}/admin/edit/release`, data);
    return result.data;
};

export const deleteRelease = async (index: number) => {
    const result: any = await axios.get(
        `${hostUrl}/admin/delete/release?index=${index}`
    );
    return result.data.result;
};

export const deleteStore = async (index: number) => {
    const result: any = await axios.get(
        `${hostUrl}/admin/delete/store?index=${index}`
    );
    return result.data.result;
};

export const addFaq = async (data: any) => {
    const result: any = await axios.post(`${hostUrl}/admin/add/faq`, data);
    return result.data;
};

export const editSneaker = async (data: any) => {
    const result: any = await axios.post(`${hostUrl}/admin/edit/sneaker`, data);
    return result.data;
};

export const getCommision = async () => {
    const result: any = await axios.get(`${hostUrl}/data/commision`);
    return result.data.result;
};

export const putCommision = async (commision: number, sale: number) => {
    const result = await axios.get(
        `${hostUrl}/admin/put/commision?commision=${commision}&sale=${sale}`
    );
    return result.data.result;
};

export const deleteSneaker = async (index: number) => {
    const result: any = await axios.get(
        `${hostUrl}/admin/delete/sneaker?index=${index}`
    );
    return result.data.result;
};

export const getTrade = async () => {
    const result: any = await axios.get(`${hostUrl}/admin/get/trade`);
    return result.data;
};

export const putStore = async (data: any) => {
    const result: any = await axios.post(`${hostUrl}/admin/put/store`, data);
    return result.data;
};

export const editStore = async (data: any) => {
    const result: any = await axios.post(`${hostUrl}/admin/edit/store`, data);
    return result.data;
};

export const getStore = async (index: number) => {
    const result: any = await axios.get(`${hostUrl}/data/store?index=${index}`);
    return result.data;
};

export const getUserList = async () => {
    const key = crypto.createHash("sha512").digest("base64");
    const result: any = await axios.post(`${hostUrl}/users`);
    return result.data;
};
