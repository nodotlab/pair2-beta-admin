export const currency = (num: string): string => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const timeBeforeMonth = (month: number): number => {
  const date: Date = new Date();
  date.setMonth(date.getMonth() - month);

  return date.getTime();
};

export const timeBeforeDate = (_date: number): number => {
  const date: Date = new Date();
  date.setDate(date.getDate() - _date);

  return date.getTime();
};

export const timeFormat = (_date: number): string => {
  const date: Date = new Date(_date);

  return `${date.getFullYear()}-${date.getMonth() + 1}/${date.getDate()}`;
};

export const timeFormat2 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);

  return `${date.getFullYear()}. ${date.getMonth() + 1}. ${date.getDate()}`;
};

export const timeFormat3 = (fullSecond: number): string => {
  const min: number = Math.floor(fullSecond / 60);
  const sec: number = fullSecond - min * 60;

  return `${min}:${sec}`;
};

export const timeFormat4 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  return `${date.getMonth() + 1}/${date.getDate()}`;
};

export const timeFormat5 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);

  let _date: string = date.getDate().toString();
  if (_date.length === 1) {
    _date = "0" + _date;
  }

  let hour: string = date.getHours().toString();
  if (hour.length === 1) {
    hour = "0" + hour;
  }

  let min: string = date.getMinutes().toString();
  if (min.length === 1) {
    min = "0" + min;
  }

  return `${date.getFullYear()}.${date.getMonth() + 1}.${_date} ${hour}:${min}`;
};

export const timeFormat6 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  return `${date.getMonth() + 1} . ${date.getDate()}`;
};
export const timeFormat7 = (timestamp: number): string => {
  const date: Date = new Date(timestamp);
  let hour: string = date.getHours().toString();
  if (hour.length === 1) {
    hour = "0" + hour;
  }

  let min: string = date.getMinutes().toString();
  if (min.length === 1) {
    min = "0" + min;
  }

  return `${hour} : ${min}`;
};

export const monthName = (timestamp: number): string => {
  const monthNames: string[] = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];
  const month = new Date(timestamp).getMonth();

  return monthNames[month];
};

export const array = (n: number, n2: number): string[] => {
  const array: string[] = [];
  for (let i = n; i < n2 + 1; ++i) {
    array.push(i.toString());
  }

  return array;
};
